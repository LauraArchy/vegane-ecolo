<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Connexion</title>
<link rel="stylesheet" href="CSS/Connexion.css">
<link href="https://fonts.googleapis.com/css?family=Signika"
	rel="stylesheet">
</head>
<body>

<img id="earth" src="images/earth.jpg" alt="earth">
	
	<header>
		<h1>Connexion à ma page personnalisée</h1>
	</header>
	
	<form method="post" action="Connexion2">
	
	<section class="infos_generales">
	<p>
	<label for="pseudo">Pseudo :</label><br>
	<input type="text" id="pseudo" name="pseudo" placeholder="Pseudo" required="required"/>
	</p>
	</section>
	<section class="infos_generales">
	<p>
	<label for="age">Mot de passe :</label><br>
	<input type="text" id="mdp" name="mdp" placeholder="Mot de passe" required="required"/>
	</p>
	</section>
	
	<input id="bouton_validation" type="submit" value="Se connecter" id="bouton_envoi"/>
	
	</form>

</body>
</html>