<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Connexion</title>
<link rel="stylesheet" href="CSS/Connexion.css">
<link href="https://fonts.googleapis.com/css?family=Signika"
	rel="stylesheet">
</head>
<body>

	<img id="earth" src="images/earth.jpg" alt="earth">
	
	<header>
		<h1>S'inscrire à Vegan-Ecolo !</h1>
	<span id="boutoninscrit">

	<a href="/Vegane-ecolo/Connexion2.jsp"><button id="boutondejainscrit">Je suis déjà inscrit</button></a>

	</span>
	</header>
	
	<form method="post" action="Connexion">
	

	<section class="infos_generales">

	<p id="introduction">
	Dîtes-nous en plus sur vous :
	</p>
	<p>
	<label for="pseudo">Pseudo :</label><br>
	<input type="text" id="pseudo" name="pseudo" placeholder="Pseudo" required="required"/>
	</p>
	<p>
	<label for="age">Mot de passe :</label><br>
	<input  type="password" id="mdp" name="mdp" placeholder="Mot de passe" required="required"/>
	</p>
		
	<p>
	<label for="age">Age :</label><br>
	<input type="text" id="age" name="age" placeholder="Âge" required="required"/>
	</p>
	<p>
	<label for="département">Département :</label><br>
	<input type="text" id="departement" name="departement" placeholder="Département" required="required"/>
	</p> 
	</section>
	
	
	<section id="infos_particulieres">
	Cochez la case qui vous correspond :<br> 
	<input type="radio" name="ideologie" value="carniste" id="carniste"/>Vous mangez des animaux (terrestres ou marins).<br>
	<input type="radio" name="ideologie" value="vg" id="vg"/>Vous êtes végétarien-ne ou végétalien-ne.<br>
	<input type="radio" name="ideologie" value="vegane" id="vegane"/>Vous êtes végane.<br>
	<p>
	<label for="animaliste">Êtes-vous militant-e animaliste? </label><br>
	<input type="radio" name="militantismeAnimaux" value="oui" id="oui"/>Oui<br>
	<input type="radio" name="militantismeAnimaux" value="non" id="non"/>Non<br>
	</p>
	<p> Êtes-vous écolo? <br> 
	<input type="radio" name="ecologie" value="non" id="non"/>Vous n'essayez pas particulièrment d'avoir moins d'impact sur l'environnement<br>
	<input type="radio" name="ecologie" value="moderement" id="moderement"/>Vous adaptez modérément votre mode de vie.<br>
	<input type="radio" name="ecologie" value="beaucoup" id="beaucoup"/>Vous faîtes beaucoup de choses pour réduire votre impact.<br>
	</p>
	<p>
	<label for="ecologiste">Êtes-vous militant-e écolo? </label><br>
	<input type="radio" name="militantismeEcolo" value="oui" id="oui"/>Oui<br>
	<input type="radio" name="militantismeEcolo" value="non" id="non"/>Non<br>
	</p>
	<input id="bouton_validation" type="submit" value="S'inscrire et se connecter" id="bouton_envoi"/> 

	</section>
	</form>
	

</body>
</html>