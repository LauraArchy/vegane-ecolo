
package servlets;

import java.io.IOException;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import model.User;

/**
 * Servlet implementation class Pouit
 */
public class Connexion extends HttpServlet {
	
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("Vegane-ecolo");			
	private EntityManager manager = emf.createEntityManager();	
	

	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Connexion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
   
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String dir = "";
		int x = 0;

		 User user = new User();
			
			HttpSession session = request.getSession();
			
			String textePseudo = request.getParameter("pseudo");
			session.setAttribute("pseudo", textePseudo);
			user.setNom(textePseudo);
			
			String motDePasse = request.getParameter("mdp");
			session.setAttribute("mdp", motDePasse);
			user.setPrenom(motDePasse);
			
			int age = Integer.parseInt(request.getParameter("age"));
			session.setAttribute("age", age);
			user.setAge(age);
			
			int numDepartement = Integer.parseInt(request.getParameter("departement"));
			session.setAttribute("departement", numDepartement);
			user.setDepartement(numDepartement);
			
			String Ideologie = request.getParameter("ideologie");
				if (Ideologie.equals("carniste")) {
					request.setAttribute("ideologie", "carniste");
					user.setRegime("carniste");
				} else if (Ideologie.equals("vg")) {
					request.setAttribute("ideologie", "vg");
					user.setRegime("végétarien");
				} else if (Ideologie.equals("vegane")) {
					request.setAttribute("ideologie", "vegane");
					user.setRegime("vegan");
				}
			
			String MilitantismeAnimaux = request.getParameter("militantismeAnimaux");
				if (MilitantismeAnimaux.equals("oui")) {
					request.setAttribute("militantismeAnimaux", "oui");
					user.setMilitantanimaliste("oui");
					x++;
				} else if (MilitantismeAnimaux.equals("non")) {
					request.setAttribute("militantismeAnimaux", "non");
					user.setMilitantanimaliste("non");
				}
				
			String Ecologie = request.getParameter("ecologie");
				if (Ecologie.equals("non")) {
					request.setAttribute("ecologie", "non");
					user.setEcologie("oui");
				} else if (Ecologie.equals("moderement")) {
					request.setAttribute("ecologie", "moderement");
					user.setEcologie("un peu");
				} else if (Ecologie.equals("beaucoup")) {
					request.setAttribute("ecologie", "beaucoup");
					user.setEcologie("non");
				}
				
			String MilitantismeEcologie = request.getParameter("militantismeEcolo");
				if (MilitantismeEcologie.equals("oui")) {
					request.setAttribute("militantismeEcolo", "oui");
					user.setMilitantecolo("oui");
				} else if (MilitantismeEcologie.equals("non")) {
					request.setAttribute("militantismeEcolo", "non");
					user.setMilitantecolo("non");
				}
				
			if (x==1) {
				dir="/template.jsp";
			} else {
				dir="/PageEcolo.jsp";
			}
			
			EntityTransaction transaction = manager.getTransaction();
			if (!transaction.isActive()) {
				transaction.begin();
				manager.persist(user);
				transaction.commit();				
			} else {
				System.err.println("CA NE MARCHE PAS");
			}
			
		


			this.getServletContext().getRequestDispatcher(dir).forward(request, response);
		
	}

}
