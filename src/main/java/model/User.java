package model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private int age;

	private int departement;

	private String ecologie;

	private String militantanimaliste;

	private String militantecolo;

	private String nom;

	private String prenom;

	private String regime;

	public User() {
	}

	public int getId() {
		return this.id;
	}

	public User(int age, int departement, String ecologie, String militantanimaliste, String militantecolo, String nom,
			String prenom, String regime) {
		super();
		this.age = age;
		this.departement = departement;
		this.ecologie = ecologie;
		this.militantanimaliste = militantanimaliste;
		this.militantecolo = militantecolo;
		this.nom = nom;
		this.prenom = prenom;
		this.regime = regime;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getDepartement() {
		return this.departement;
	}

	public void setDepartement(int departement) {
		this.departement = departement;
	}

	public String getEcologie() {
		return this.ecologie;
	}

	public void setEcologie(String ecologie) {
		this.ecologie = ecologie;
	}

	public String getMilitantanimaliste() {
		return this.militantanimaliste;
	}

	public void setMilitantanimaliste(String militantanimaliste) {
		this.militantanimaliste = militantanimaliste;
	}

	public String getMilitantecolo() {
		return this.militantecolo;
	}

	public void setMilitantecolo(String militantecolo) {
		this.militantecolo = militantecolo;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getRegime() {
		return this.regime;
	}

	public void setRegime(String regime) {
		this.regime = regime;
	}

}